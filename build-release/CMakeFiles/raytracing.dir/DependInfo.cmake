# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/brandon/Documents/CSC418/a3/computer-graphics-ray-tracing/main.cpp" "/home/brandon/Documents/CSC418/a3/computer-graphics-ray-tracing/build-release/CMakeFiles/raytracing.dir/main.cpp.o"
  "/home/brandon/Documents/CSC418/a3/computer-graphics-ray-tracing/src/DirectionalLight.cpp" "/home/brandon/Documents/CSC418/a3/computer-graphics-ray-tracing/build-release/CMakeFiles/raytracing.dir/src/DirectionalLight.cpp.o"
  "/home/brandon/Documents/CSC418/a3/computer-graphics-ray-tracing/src/PointLight.cpp" "/home/brandon/Documents/CSC418/a3/computer-graphics-ray-tracing/build-release/CMakeFiles/raytracing.dir/src/PointLight.cpp.o"
  "/home/brandon/Documents/CSC418/a3/computer-graphics-ray-tracing/src/blinn_phong_shading.cpp" "/home/brandon/Documents/CSC418/a3/computer-graphics-ray-tracing/build-release/CMakeFiles/raytracing.dir/src/blinn_phong_shading.cpp.o"
  "/home/brandon/Documents/CSC418/a3/computer-graphics-ray-tracing/src/raycolor.cpp" "/home/brandon/Documents/CSC418/a3/computer-graphics-ray-tracing/build-release/CMakeFiles/raytracing.dir/src/raycolor.cpp.o"
  "/home/brandon/Documents/CSC418/a3/computer-graphics-ray-tracing/src/reflect.cpp" "/home/brandon/Documents/CSC418/a3/computer-graphics-ray-tracing/build-release/CMakeFiles/raytracing.dir/src/reflect.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "../eigen"
  "../json"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/brandon/Documents/CSC418/a3/computer-graphics-ray-tracing/build-release/CMakeFiles/hw2.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
