#include "blinn_phong_shading.h"
#include "first_hit.h"
#include <iostream>

Eigen::Vector3d blinn_phong_shading(
  const Ray & ray,
  const int & hit_id, 
  const double & t,
  const Eigen::Vector3d & n,
  const std::vector< std::shared_ptr<Object> > & objects,
  const std::vector<std::shared_ptr<Light> > & lights)
{
    Eigen::Vector3d ka(0,0,0);Eigen::Vector3d kd(0,0,0);Eigen::Vector3d ks(0,0,0);
    Eigen::Vector3d d;
    double max_t;
    Eigen::Vector3d q = ray.origin + t*ray.direction;

    
    for (int i; i< lights.size(); i++) {

        lights[i]->direction(q, d, max_t);
        
        double shadow;
        int a;
        Eigen::Vector3d b;
        Ray light;
        light.origin = q;
        light.direction = d;
        
        if (!first_hit(light, 0.000000001, objects, a , shadow, b) || shadow >= max_t) {
            Eigen::Vector3d l = d.normalized();
            const Eigen::Vector3d v = (-ray.direction).normalized();
            const Eigen::Vector3d h = (v+l).normalized();
            //diffuse
            kd = kd + (objects[hit_id]->material->kd.array() *fmax(n.dot(l),0) * lights[i]->I.array()).matrix();
            //specular
            ks = ks + (objects[hit_id]->material->ks.array()  * pow(fmax(n.dot(h),0), objects[hit_id]->material->phong_exponent) * lights[i]->I.array()).matrix();
        }
    }
    //ambient
    ka = (objects[hit_id]->material->ka.array() * Eigen::Vector3d(0.1, 0.1, 0.1).array()).matrix();
    return ka + kd + ks;
}
