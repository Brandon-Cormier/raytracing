#include "viewing_ray.h"
#include <Eigen/Geometry>


void viewing_ray(
  const Camera & camera,
  const int i,
  const int j,
  const int width,
  const int height,
  Ray & ray)
{
  ////////////////////////////////////////////////////////////////////////////
  // Add your code here
  ////////////////////////////////////////////////////////////////////////////
  ray.origin = camera.e;
  
  //u and v are calculated, then used to calculate the direction of the ray
  double u =  camera.width*(j + 0.5)/width - camera.width/2;
  double v =  camera.height*(i + 0.5)/height - camera.height/2;

  ray.direction = u*camera.u - v*camera.v - camera.d*camera.w;

}